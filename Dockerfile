FROM docker-registry-default.apps.appcanvas.net/memei-development/memei-java:8

LABEL MANTAINER="Serasa Experian IT Team"

# Receive JAR File Full Path on Docker Build Args
ARG JAR_FILE

# Environment Variable setg Spring Profile
ENV SPRING_PROFILE deploy

# Environment variable Log Level
ENV SPRING_LOGGING_LEVEL INFO

ENV API_VERSION /application/v1

ENV APPLICATION_NAME application_name

ENV REPORT_PATH test_cucumber

ENV FEATURE_SOURCE ${APPLICATION_NAME}/src/test/resources/features/

# Pass JAR File Full Path on Docker run command
ENV JAR_FILE ${JAR_FILE}

# Expose port variable
ENV PORT 8080

# Set XMS JVM Value on Docker run
ENV JVM_XMS 512m

# Set XMX JVM Value on Docker run
ENV JVM_XMX 1024m

# Add jar file to image on build
ADD ${JAR_FILE} /app/app.jar

USER nobody

# The entry point to exec java jar
CMD java -Xms${JVM_XMS} -Xmx${JVM_XMX} -Dspring.profiles.active=${SPRING_PROFILE} -DAPI_VERSION=${API_VERSION} -DFEATURE_SOURCE=${FEATURE_SOURCE} -DREPORT_PATH=${REPORT_PATH} -jar /app/app.jar