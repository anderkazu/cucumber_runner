package br.com.experian.cucumber.integration.cucumber.steps;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.entity.StringEntity;

public abstract class MainSteps {

	public String path = "";
	public List<Header> headers = new ArrayList<>();
	public List<NameValuePair> parameters = new ArrayList<>();
	public String jsonBodyString = null;
	public StringEntity entity = null;

	public HttpResponse response;
	public String responseJson;

	public static LinkedHashMap<String, String> userParameters = new LinkedHashMap<String, String>();
	public String accessToken = "";
    public String domain = "";

    public String collection = "";
    public String jsonDocument = "";
    public String column = "";
    public String value = "";
    public String apiVersion = "";
    
    //Dados MongoDB 
    public String host;
    public int port;
    public String database;
    
	public void init() {
		path = "";
		headers = new ArrayList<>();
		parameters = new ArrayList<>();
		jsonBodyString = "";
		entity = null;
		userParameters = new LinkedHashMap<String, String>();
	}
	
	public String replaceVariablesValues(String text) throws Throwable {
		String rx = "(\\$\\{[^}]+\\})";

		StringBuffer sb = new StringBuffer();
		Pattern p = Pattern.compile(rx);
		Matcher m = p.matcher(text);

		while (m.find()) {
			// Avoids throwing a NullPointerException in the case that you
			// Don't have a replacement defined in the map for the match
			String repString = userParameters.get(m.group(1));
			if (repString != null)
				m.appendReplacement(sb, repString);
		}
		m.appendTail(sb);

		return sb.toString();
	}
	
	public void extractJsonResponse() throws Throwable {
        Integer statusCode = response.getStatusLine().getStatusCode();

        if (response.getEntity() != null && response.getEntity().getContent() != null) {
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent(),"UTF-8"));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            responseJson = result.toString();
        } else {
            responseJson = "";
        }

    }

	public Object generateObjectValue(String value) throws Exception {

		if (isNumericValue(value)) {
			return Long.parseLong(value);
		}

		if (isDecimalValue(value)) {
			return Double.parseDouble(value);
		}

		if (isISODateValue(value)) {
			return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(value);
		}

		return value;
	}

	private boolean isNumericValue(String value) {

		try {
			Long.parseLong(value);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}

	}

	private boolean isDecimalValue(String value) {

		try {
			Double.parseDouble(value);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}

	}

	private boolean isISODateValue(String val) {

		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			sdf.parse(val);
			return true;
		} catch (ParseException e) {
			return false;
		}
	}

	public void removeHeader(String headerKey) {

		for (Iterator<Header> i = headers.iterator(); i.hasNext();) {
			Header header = i.next();
			if (header.getName().equals(headerKey)) {
				i.remove();
			}
		}
	}

}
