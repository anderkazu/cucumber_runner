package br.com.experian.cucumber.integration.cucumber.steps;

import static br.com.experian.cucumber.integration.cucumber.common.utils.PropertiesUtil.getProperty;
import static org.assertj.core.api.Assertions.fail;
import static org.assertj.core.api.Assertions.not;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.StringReader;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.TimeUnit;

import cucumber.api.DataTable;
import org.apache.commons.lang.StringUtils;
import org.apache.http.Header;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.bson.Document;
import org.bson.types.ObjectId;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import br.com.experian.cucumber.integration.cucumber.common.utils.JwtUtil;
import br.com.experian.cucumber.integration.cucumber.common.utils.httpclient.HttpClient;
import br.com.experian.cucumber.integration.cucumber.mongo.MongoRepository;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.json.JSONArray;
import org.json.JSONObject;

public class RestAPISteps extends MainSteps {
	private String id;
	private String token;
	private String refreshToken;
	private String accessToken;
	
	@Then("I clean JWT Token")
    public void cleanJsonToken() throws Throwable {
        accessToken = null;
        headers = new ArrayList<>();
        Hooks.scenario.write("Bearer " + accessToken);
    }
	
	@Then("^I remove the business \"([^\"]*)\" from user \"([^\"]*)\"$")
    public void iRemoveTheBusinessFromUser(String businessId, String userId) throws Throwable {
    	businessId = replaceVariablesValues(businessId);
    	userId = replaceVariablesValues(userId);
        new MongoRepository().removeUserCompany(userId, businessId);
    }
	
	@When("^I save the random \"([^\"]*)\" as \"([^\"]*)\"$")
    public void saveRandomValue(String type, String userKey) throws Throwable {
        String value;
        if (type.toUpperCase().equals("NUMBER")) {
            value = generateRandomNumber();
        } else {
            value = generateRandomString();
        }
        userParameters.put("${" + userKey + "}", value);

        Hooks.scenario.write(userParameters.get("${" + userKey + "}"));
    }
	
	@Then("the JWT must have \"([^\"]*)\" as \"([^\"]*)\"$")
    public void valdiateStringJwt(String key, String value) throws Throwable {

        DecodedJWT jwt = JWT.decode(accessToken.replaceAll("Bearer ", ""));
        String jwtValue = "";
        List<String> jwtListValue = new ArrayList<String>();
        switch (key) {
            case "exp":
            case "expires_in":
            case "iat":
                jwtValue = getLongClaim(jwt, key);
                break;
            case "scope":
            case "authorities":
                jwtListValue = getListClaim(jwt, key);
                break;
            default:
                jwtValue = getStringClaim(jwt, key);
        }

        if (value.contains("[")) {
            String[] listValues = value.replaceAll("\\[", "").replaceAll("\\]", "").split(",");
            for (String listValue : listValues) {
                assertEquals("Expected value \'" + listValue.trim() + "\' not found on " + jwtListValue.toString(), jwtListValue.contains(listValue.trim()), true);
            }
        } else {
            assertEquals(jwtValue, value);
        }
    }

    @Then("the JWT must NOT have \"([^\"]*)\" as \"([^\"]*)\"$")
	public void valdiateStringNotPresentJwt(String key, String value) throws Throwable {

		DecodedJWT jwt = JWT.decode(accessToken.replaceAll("Bearer ", ""));
		String jwtValue = "";
		List<String> jwtListValue = new ArrayList<String>();
		switch (key) {
			case "exp":
			case "expires_in":
			case "iat":
				jwtValue = getLongClaim(jwt, key);
				break;
			case "scope":
			case "authorities":
				jwtListValue = getListClaim(jwt, key);
				break;
			default:
				jwtValue = getStringClaim(jwt, key);
		}

		if (value.contains("[")) {
			String[] listValues = value.replaceAll("\\[", "").replaceAll("\\]", "").split(",");
			for (String listValue : listValues) {
				assertNotEquals("Expected value \'" + listValue.trim() + "\' not found on " + jwtListValue.toString(), jwtListValue.contains(listValue.trim()), true);
			}
		} else {
			assertNotEquals(jwtValue, value);
		}
	}
	
	@Then("the response JSON node \"([^\"]*)\" must \"([^\"]*)\"$")
    public void validateJsonNode(String key, String value) throws Throwable {
        if (!"".equals(responseJson)) {
            JsonNode rootNode = new ObjectMapper().readTree(new StringReader(responseJson));
            if (value.equals("exist")) {
            	Hooks.scenario.write("Value found: " + rootNode.at(key).asText());
                assertNotEquals("", rootNode.at(key).asText());
            } else {
                assertEquals("", rootNode.at(key).asText());
            }
        }
    }

	@Then("the response Header \"([^\"]*)\" must have \"([^\"]*)\"$")
	public void validateResponseHeader(String key, String value) throws Throwable {
		String[] listValues = value.split(",");
		String responseHeader = response.getHeaders(key)[0].getValue();
		for (String listValue : listValues) {
			assertTrue("Expected value \'" + listValue.trim() + "\' not found on " + responseHeader, responseHeader.contains(listValue.trim()));
		}
	}

	@Then("the response Header \"([^\"]*)\" must do not have \"([^\"]*)\"$")
	public void validateResponseHeaderDoNot(String key, String value) throws Throwable {
		String[] listValues = value.split(",");
		String responseHeader = response.getHeaders(key)[0].getValue();
		for (String listValue : listValues) {
			assertFalse("Expected value \'" + listValue.trim() + "\' not found on " + responseHeader, responseHeader.contains(listValue.trim()));
		}
	}
	
	@Given("^I set the Basic Authorization header with clientId \"([^\"]*)\" and clientSecret \"([^\"]*)\"$")
    public void setBasicAuthClientHeader(String user, String pwd) throws Throwable {
    	setBasicAuthHeader(user, pwd);
    }
	
	@Given("^I clear all the headers$")
    public void clearAllHeaders() throws Throwable {
        this.headers = new ArrayList<>();
    }
	
	@Then("^I put this id \"([^\"]*)\" on group \"([^\"]*)\"$")
    public void putIdOnGroup(String id, String groupName) throws Throwable {
        new MongoRepository().addToGroup(groupName, id);
    }

    @Then("^I remove this id \"([^\"]*)\" from group \"([^\"]*)\"$")
    public void iRemoveThisIdFromGroup(String id, String groupName) throws Throwable {
       new MongoRepository().removeToGroup(groupName, id);
    }

    @Then("^I put this id \"([^\"]*)\" on business group \"([^\"]*)\" from this business id \"([^\"]*)\"$")
    public void iPutThisIdOnBusinessGroupFromThisBusinessId(String userId, String groupName, String businessId) throws Throwable {
        new MongoRepository().addBusinessGroup(businessId, groupName, userId);
    }
   
	@Given("^I use the id \"([^\"]*)\"$")
    public void setId(String id) throws Throwable{
    	this.id = replaceVariablesValues(id);
    	Hooks.scenario.write(this.id);
    }
	
	@When("^I find the validation token by id$")
    public void findValidationToken() throws Throwable{
    	MongoRepository mongoRepository = new MongoRepository();
    	this.token = mongoRepository.findTokenByUserAccountId(id);
    	Hooks.scenario.write(token);
    }
    
    @When("^I find the validation token by userId$")
    public void findValidationTokenByUserId() throws Throwable{
    	MongoRepository mongoRepository = new MongoRepository();
    	this.token = mongoRepository.findTokenByUserId(id);
    	Hooks.scenario.write(token);
    }
    
    @Then("^I print the token$")
    public void printToken() throws Throwable{
    	Hooks.scenario.write(token);
    }
    
    @When("^I save the token value as \"([^\"]*)\"$")
    public void saveTokenValue(String userKey) throws Throwable {
        if (System.getProperty("environment.name") != null && (System.getProperty("environment.name").equals("LO")
                        || System.getProperty("environment.name").equals("ST")
                        || System.getProperty("environment.name").equals("QA"))) {
            userParameters.put("${" + userKey + "}", "123456");
        } else {
            userParameters.put("${" + userKey + "}", token);
        }
        Hooks.scenario.write(userParameters.get("${" + userKey + "}"));
    }
	
	@Given("I run the test case \"([^\"]*)\"$")
	public void setTestCase(String testCase) throws Throwable {
	}

	@Given("^I use the route \"([^\"]*)\"$")
	public void setRoute(String route) throws Throwable {
		route = replaceVariablesValues(route);
		route = route.replaceAll(" ", "%20");
		path = route;
	}

	@Given("^I set queryparameter \"([^\"]*)\" as \"([^\"]*)\"$")
	public void setQueryParameter(String key, String value) throws Throwable {
		parameters.add(new BasicNameValuePair(key, value));
	}

	@Given("^I set pathparameter \"([^\"]*)\" as \"([^\"]*)\"$")
	public void setPathParameter(String key, String value) throws Throwable {
		path = path.replaceAll(key, value);
	}


	@Given("^I set header \"([^\"]*)\" as \"([^\"]*)\"$")
	public void setHeader(String key, String value) throws Throwable {
		for (Header header : headers) {
			if (header.getName().equals(key)) {
				headers.remove(header);
				break;
			}
		}
		headers.add(new BasicHeader(key, value));
	}

	@Given("^I use the json body$")
	public void setJsonBody(String jsonBody) throws Throwable {

		// replace variable values
		jsonBodyString = replaceVariablesValues(jsonBody);
		Hooks.scenario.write(jsonBodyString);

	}
	
    @Given("^I use the collection \"([^\"]*)\"$")
    public void setCollection(String collection) throws Throwable {
        this.collection = collection;
    }
    
    @Given("^I use the json document$")
    public void setJsonDocument(String jsonBody) throws Throwable {
        jsonDocument = replaceVariablesValues(jsonBody);
        Hooks.scenario.write(jsonDocument);
    }
    
    @Given("^I use the column document \"([^\"]*)\"$")
    public void setColumn(String column) throws Throwable {
        this.column = replaceVariablesValues(column);
        Hooks.scenario.write(this.column);
    }

    @Given("^I use the value document \"([^\"]*)\"$")
    public void setValueDocument(String value) throws Throwable {
        this.value = replaceVariablesValues(value);
        Hooks.scenario.write(this.value);
    }

	@When("^I send the POST request$")
	public void postRequest() throws Throwable {
		HttpClient http = new HttpClient(domain, this.apiVersion);
		entity = new StringEntity(jsonBodyString, ContentType.APPLICATION_JSON);
		response = http.sendPost(path, parameters, headers, entity);
		extractJsonResponse();
        extractResponseTokens();
	}

	@When("^I send the GET request$")
	public void getRequest() throws Throwable {
		HttpClient http = new HttpClient(domain, this.apiVersion);
		response = http.sendGet(path, parameters, headers);
		extractJsonResponse();
		extractResponseTokens();
	}

	@When("^I send the PUT request$")
 	public void putRequest() throws Throwable {
		HttpClient http = new HttpClient(domain, this.apiVersion);
		entity = new StringEntity(jsonBodyString, ContentType.APPLICATION_JSON);
		response = http.sendPut(path, parameters, headers, entity);
		extractJsonResponse();
		extractResponseTokens();
	}

	@When("^I send the DELETE request$")
	public void deleteRequest() throws Throwable {
		HttpClient http = new HttpClient(domain, this.apiVersion);
		response = http.sendDelete(path, parameters, headers);
		extractJsonResponse();
		extractResponseTokens();
	}

	@When("^I send the PATCH request$")
	public void patchRequest() throws Throwable {
		HttpClient http = new HttpClient(domain, this.apiVersion);
		entity = new StringEntity(jsonBodyString, ContentType.APPLICATION_JSON);
		response = http.sendPatch(path, parameters, headers, entity);
		extractJsonResponse();
		extractResponseTokens();
	}

	@When("^I send the OPTIONS request$")
	public void optionsRequest() throws Throwable {
		HttpClient http = new HttpClient(domain, this.apiVersion);
		response = http.sendOptions(path, parameters, headers);
		extractJsonResponse();
		extractResponseTokens();
	}
	
    @When("^I save the document$")
    public void saveDocument() throws Throwable {
        MongoRepository mongoRepository = new MongoRepository();
        mongoRepository.save(this.collection, this.jsonDocument);
    }

    @Then("the response JSON must have \"([^\"]*)\" as the String \"([^\"]*)\"$")
    public void valdiateStringJsonBody(String key, String value) throws Throwable {
        if (!"".equals(responseJson)) {
            JsonNode rootNode = new ObjectMapper().readTree(new StringReader(responseJson));

            if (!key.equals("size")) {
                Hooks.scenario.write("Value found: " + rootNode.at(key).asText());
                assertEquals(value.trim(), rootNode.at(key).asText().trim());
            } else {
                Hooks.scenario.write("Value found: " + value);

                if (rootNode == null) {
                    assertEquals(Integer.parseInt(value), 0);
                } else {
                    assertEquals(Integer.parseInt(value), rootNode.size());
                }
            }
        }
    }

	@Then("the response JSON Array \"([^\"]*)\" must contain \"([^\"]*)\" as \"([^\"]*)\" and the key \"([^\"]*)\" as the String \"([^\"]*)\"$")
	public void validateStringArrayJsonBody(String arrayKey, String containedKey, String containedValue, String key,
			String value) throws Throwable {

		if (!"".equals(responseJson)) {

			JsonNode rootNode = new ObjectMapper().readTree(new StringReader(responseJson));

			JsonNode arrayNode = rootNode.at(arrayKey);
			if (arrayNode.isArray()) {

				for (JsonNode node : arrayNode) {

					String valueToCompare = node.path(containedKey).asText();

					if ("".equals(containedValue)) {
						fail("The value from " + containedKey + " cannot be empty!");
					}

					if ("".equals(valueToCompare)) {
						fail("The key " + containedKey + " doen't exists in the JSON Array Object!");
					}

					if (containedValue.contains(valueToCompare)) {

						String returnedValue = node.path(key).asText();

						Hooks.scenario
								.write("The value from key \"" + containedKey + "\" is \"" + containedValue + "\"");
						Hooks.scenario.write("The value from key \"" + key + "\" is \"" + returnedValue + "\"");

						assertEquals(value, returnedValue);
					}
				}
			} else {
				fail("The param " + arrayKey + " is not an Array!");
			}
		}
	}

	@When("^I save the response value \"([^\"]*)\" as \"([^\"]*)\"$")
	public void saveJsonValue(String jsonKey, String userKey) throws Throwable {
		JsonNode rootNode = new ObjectMapper().readTree(new StringReader(responseJson));

		String jsonValue = rootNode.at(jsonKey).asText();
		userParameters.put("${" + userKey + "}", jsonValue);

		Hooks.scenario.write(userParameters.get("${" + userKey + "}"));
	}

	@When ("^I set the JSON list \"([^\"]*)\" with keys and values as$")
	public void registerUser(String userKey, DataTable table) throws Throwable {
		JSONArray jsonArray = new JSONArray();
		List<String> headers = table.raw().get(0);

		for (Map<String, String> item : table.asMaps(String.class, String.class)) {
			JSONObject line = new JSONObject();
			for (String header : headers) {
				if (header.endsWith(".toNumber")){
					line.put(header.replaceAll(".toNumber", ""), Long.parseLong(item.get(header)));
				}else {
					line.put(header, item.get(header));
				}
			}
			jsonArray.put(line);
		}

		userParameters.put("${" + userKey + "}", jsonArray.toString());

		Hooks.scenario.write(userParameters.get("${" + userKey + "}"));
	}

	@When("^I delete the document$")
    public void deleteDocument() throws Throwable{
    	MongoRepository mongoRepository = new MongoRepository();

    	if(column.equalsIgnoreCase("_id")){
    		mongoRepository.deleteOne(this.collection, new Document(column, new ObjectId(value)));
    	}else{
    		mongoRepository.deleteMany(this.collection, new Document(column, value));
    	}
    }

	@And("^is an illegal GET request$")
	public void isIllegalRequest() throws Throwable {
		try {
			HttpClient http = new HttpClient(domain, this.apiVersion);
			response = http.sendGet(path, parameters, headers);
			extractJsonResponse();
		} catch (Exception e) {
			Hooks.add(e);
		}
	}

    @And("^is an illegal POST request$")
    public void isIllegalPostRequest() throws Throwable {
    	try {
    		HttpClient http = new HttpClient(domain, this.apiVersion);
    		entity = new StringEntity(jsonBodyString, ContentType.APPLICATION_JSON);
            response = http.sendPost(path, parameters, headers, entity);
            extractJsonResponse();
    	}
    	catch(Exception e) {
    		Hooks.add(e);
    	}
    }

	@Then("the response JSON must have \"([^\"]*)\" as the Integer (\\d+)$")
	public void valdiateIntegerJsonBody(String key, Integer value) throws Throwable {
		JsonNode rootNode = new ObjectMapper().readTree(new StringReader(responseJson));

		Hooks.scenario.write("Value found: " + rootNode.at(key).asInt());

		assertEquals(value.intValue(), rootNode.at(key).asInt());
	}

	@Then("^I print the path$")
	public void pathToReport() throws Throwable {
		Hooks.scenario.write(this.getCompletePath());
		Hooks.scenario.write(headers.toString());
		Hooks.scenario.write(parameters.toString());
	}

	@Then("^I print the response$")
	public void responseToReport() throws Throwable {

		Hooks.scenario.write(response.getStatusLine().toString());
		String headers = "[";
		for (Header header : response.getAllHeaders()) {
			headers += header.toString() + ", ";
		}
		Hooks.scenario.write(headers + "]");

		try {
			ObjectMapper mapper = new ObjectMapper();
			Object json = mapper.readValue(responseJson, Object.class);
			String prettyJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
			Hooks.scenario.write(prettyJson);
		} catch (Exception e) {

		}
	}

	@Then("^I print the error$")
	public void responseToError() throws Throwable {

		Hooks.scenario.write(Hooks.getExceptions().get(0).getMessage());

		try {
			ObjectMapper mapper = new ObjectMapper();
			Object json = mapper.readValue(responseJson, Object.class);
			String prettyJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
			Hooks.scenario.write(prettyJson);
		} catch (Exception e) {

		}
	}

	@Then("^Http response should be (\\d+)$")
	public void validateResponse(Integer responseCode) throws Throwable {
		Integer statusCode = response.getStatusLine().getStatusCode();
		assertEquals(responseCode.toString(), statusCode.toString());
	}

	@When("A failure is expected")
	public void expectException() {
		Hooks.expectException();
	}

	@And("it fails")
	public void it_fails() {
		assertThat(Hooks.getExceptions(), is(not(empty())));
	}

	@Given("^I generate a milliseconds date \"([^\"]*)\" with \"([^\"]*)\" minutes$")
	public void generateMillisecondsWithMinutes(String userKey, String minutes) throws Throwable {
		Long dateInMilis = (new Date().getTime() / 1000) + (Integer.parseInt(minutes) * 60);

		userParameters.put("${" + userKey + "}", dateInMilis.toString());
	}

	@Given("^I set the Bearer Authorization header$")
	public void setBearerAuthHeader() throws Throwable {
		for (Header header : headers) {
			if (header.getName().equals("Authorization")) {
				headers.remove(header);
				break;
			}
		}
		headers.add(new BasicHeader("Authorization", "Bearer " + accessToken));
	}

    @When("^I print the JWT access token$")
    public void decodeJWT() throws Throwable {
    	Hooks.scenario.write("token: " + accessToken.replaceAll("Bearer ", ""));

        DecodedJWT jwt = JWT.decode(accessToken.replaceAll("Bearer ", ""));

        Hooks.scenario.write("exp: " + getLongClaim(jwt, "exp"));
//    	Hooks.scenario.write("expires_in: " + getLongClaim(jwt, "expires_in"));
        Hooks.scenario.write("jti: " + getStringClaim(jwt, "jti"));

        Hooks.scenario.write("scope: " + getListClaim(jwt, "scope").toString());
        Hooks.scenario.write("authorities: " + getListClaim(jwt, "authorities").toString());
        Hooks.scenario.write("iat: " + getLongClaim(jwt, "iat"));
        //Hooks.scenario.write(Integer.toString(getIntClaim(jwt, "exp") - getIntClaim(jwt, "iat")) );
        Hooks.scenario.write("user_id: " + getStringClaim(jwt, "user_id"));
        Hooks.scenario.write("client_id: " + getStringClaim(jwt, "client_id"));
    }

    @Given("^I generate a miliseconds date \"([^\"]*)\" with \"([^\"]*)\" minutes$")
    public void generateMilisecondsWithMinutes(String userKey, String minutes) throws Throwable {
        Long dateInMilis = (new Date().getTime() / 1000) + (Integer.parseInt(minutes) * 60);

        userParameters.put("${" + userKey + "}", dateInMilis.toString());
    }
    
	@Given("^I use the json and generate a JWT Token")
	public void setJsonToken(String jsonToken) throws Throwable {
		jsonToken = replaceVariablesValues(jsonToken);
		Hooks.scenario.write(jsonToken);
		jsonToken = jsonToken.replace("\n", "").replace("\r", "").replaceAll(" ", "");
		accessToken = new JwtUtil().createJwt(jsonToken);
	}

	@Given("^I clear the Authorization header$")
	public void clearAuthHeader() throws Throwable {
		removeHeader("Authorization");
	}
	
	@Then("Stop \"([^\"]*)\" seconds$")
    public void stopSeconds(String second) throws Throwable {
    	TimeUnit.SECONDS.sleep(Long.valueOf(second));
    }
	
	@Then("^The variable \"([^\"]*)\" equals \"?([^\"]*)\"?$")
	public void a(String variable, String value) throws Throwable{
		variable = replaceVariablesValues(variable);
		
		assertEquals(variable, value);
	}

    @Given("^I use the environment \"([^\"]*)\"$")
    public void setEnvironment(String environment) throws Throwable {
        domain = replaceVariablesValues(environment);
    }

    @Then("the response JSON must have \"([^\"]*)\" as a not empty String$")
    public void validateNotEmptyStringJsonBody(String key) throws Throwable {

        if (!"".equals(responseJson)) {
            JsonNode rootNode = new ObjectMapper().readTree(new StringReader(responseJson));

            String valueReturned = rootNode.at(key).asText();
			Hooks.scenario.write("Value found: " + valueReturned);

            if(StringUtils.isEmpty(valueReturned)) {
            	fail("The value from " + key + " must not be empty!");
            }
        }
    }

    @Given("^I set the Bearer Authorization header as \"([^\"]*)\"$")
    public void setBearerAuthHeaderAs(String accessToken) throws Throwable {
    	
    	for (Header header: headers) {
    		if (header.getName().equals("Authorization")) {
    		   	headers.remove(header);
    		   	break;
    		}
    	}
    	
        headers.add(new BasicHeader("Authorization", "Bearer " + accessToken));
    }

    @Given("^I set the Basic Authorization header with user \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void setBasicAuthHeader(String user, String pwd) throws Throwable {
        user = replaceVariablesValues(user);
        pwd = replaceVariablesValues(pwd);
        String tokenValue = "Basic " + Base64.getEncoder().encodeToString((user + ":" + pwd).getBytes());
        removeHeader("Authorization");
        headers.add(new BasicHeader("Authorization", tokenValue));
    }

    @Given("^I use new url request \"([^\"]*)\"$")
    public void newUrl(String apiVersion) throws Throwable {
    	this.apiVersion = apiVersion;
    }
    
    @Given("^I use the domain \"([^\"]*)\"$")
    public void setDomain(String domain) throws Throwable {
        this.domain = domain;
    }
    
    @When("^I save the variable \"([^\"]*)\" with value \"([^\"]*)\"$")
    public void saveValue(String userKey, String value) throws Throwable {
        userParameters.put("${" + userKey + "}", value);

        Hooks.scenario.write(userParameters.get("${" + userKey + "}"));
    }
    
    @When("^I save the variable \"([^\"]*)\" with value \"([^\"]*)\" as Base64$")
    public void saveBase64Value(String userKey, String value) throws Throwable {
        userParameters.put("${" + userKey + "}", Base64.getEncoder().encodeToString((value).getBytes()));
        Hooks.scenario.write(userParameters.get("${" + userKey + "}"));
    }
    
    @Then("^The response JSON can not contain the value \"([^\"]*)\"$")
    public void theResponseJSONCanNotContainTheValue(String value) throws Throwable {
        if (!"".equals(responseJson)) {
        	ObjectMapper mapper = new ObjectMapper();
            JsonNode rootNode = mapper.readTree(new StringReader(responseJson));
            if(rootNode.isArray()){
                Iterator<JsonNode> elementsIterator = ((ArrayNode) rootNode).elements();
                while (elementsIterator.hasNext()) {
                	JsonNode node = elementsIterator.next();
                	boolean hasService = false;
                	Iterator<String> elementsName = node.fieldNames();
                	
                	while(elementsName.hasNext()){
                		if(node.get(elementsName.next()).equals(value)){
                			hasService = true;
                		}
                	}
                	assertFalse(hasService);
                 }
            }else{
            	assertFalse(rootNode.toString().contains(value));
            }
        }
    }

	@Then("^The response JSON must contain the key \"([^\"]*)\" and value \"([^\"]*)\" in the list node \"([^\"]*)\"$")
	public void theResponseJSONMustContainKeyValue(String key, String value, String listNodeName) throws Throwable {
		key = replaceVariablesValues(key);
		value = replaceVariablesValues(value);
		if (!"".equals(responseJson)) {
			ObjectMapper mapper = new ObjectMapper();
			JsonNode rootNode = mapper.readTree(new StringReader(responseJson));
			JsonNode list = rootNode.at(listNodeName);

			if(list.isArray()){
				Iterator<JsonNode> elementsIterator = ((ArrayNode) list).elements();
				while (elementsIterator.hasNext()) {
					JsonNode node = elementsIterator.next();
					//"id": "100226",
					//"value": "1519349306"
					String idReturned = node.at("/id").asText();
					if (key.equals(idReturned)) {
						String valueReturned = node.at("/value").asText();
						assertEquals(value, valueReturned);
					}
				}
			}else{
				fail("The value from " + key + " must not be empty!");
			}
		}
	}

	@Then("^The response JSON must contain the key \"([^\"]*)\" with value not null in the list node \"([^\"]*)\"$")
	public void theResponseJSONMustContainKeyValue(String key, String listNodeName) throws Throwable {
		key = replaceVariablesValues(key);
		value = replaceVariablesValues(value);
		if (!"".equals(responseJson)) {
			ObjectMapper mapper = new ObjectMapper();
			JsonNode rootNode = mapper.readTree(new StringReader(responseJson));
			JsonNode list = rootNode.at(listNodeName);
			Boolean found = false;

			if(list.isArray()){
				Iterator<JsonNode> elementsIterator = ((ArrayNode) list).elements();
				while (elementsIterator.hasNext()) {
					JsonNode node = elementsIterator.next();
					//"id": "100226",
					//"value": "1519349306"

					String idReturned = node.at("/id").asText();
					if (key.equals(idReturned)) {
						String valueReturned = node.at("/value").asText();
						Hooks.scenario.write("Value found: " + valueReturned);
						found = true;
						if (StringUtils.isEmpty(valueReturned)) {
							fail("The value from " + key + " must not be empty!");
							break;
						}
					}
				}
			}else{
				fail("The value from " + key + " must not be empty!");
			}
			assertEquals("the key '" + key + "' was not found on the response", found, true);
		}
	}
    
    @Then("the response JSON must have \"([^\"]*)\" not equals \"([^\"]*)\"$")
    public void validateDifferentStringJsonBody(String key, String value) throws Throwable {
    	value = replaceVariablesValues(value);
    	if (!"".equals(responseJson)) {
            JsonNode rootNode = new ObjectMapper().readTree(new StringReader(responseJson));

            Hooks.scenario.write("Value found: " + rootNode.at(key).asText());
            assertNotEquals(value, rootNode.at(key).asText());
        }
    }
    
    @Then("^the response JSON must have the property \"([^\"]*)\" with values \"([^\"]*)\"$")
    public void theResponseJSONMustHaveThePropertyWithValues(String attribute, String values) throws Throwable {

        List<String> strings = Arrays.asList(values.split(","));

        if (!"".equals(responseJson)) {
            JsonNode rootNode = new ObjectMapper().readTree(new StringReader(responseJson));

            strings.forEach(value -> {
                assertTrue(rootNode.toString().contains(value));
            });
        }
    }

    @Then("^I extract the JWT access token$")
	public void extractJwtAccessToken() throws Throwable {
		extractAccessToken();
		setBearerAuthHeader();
	}

    
    private String getStringClaim(DecodedJWT jwt, String claimName) {
        if (jwt.getClaim(claimName).isNull()) {
            return "";
        } else {
            return jwt.getClaim(claimName).asString();
        }
    }

    private String getLongClaim(DecodedJWT jwt, String claimName) {
        if (jwt.getClaim(claimName).isNull()) {
            return "0";
        } else {
            return jwt.getClaim(claimName).asLong().toString();
        }
    }

    private List<String> getListClaim(DecodedJWT jwt, String claimName) {
        if (jwt.getClaim(claimName).isNull()) {
            return new ArrayList<String>();
        } else {
            return jwt.getClaim(claimName).asList(String.class);
        }
    }

	private void extractAccessToken() {
		if (response.getHeaders("Authorization").length > 0) {
			accessToken = response.getHeaders("Authorization")[0].getValue();
		} else {
			accessToken = "";
		}

		if ("".equals(accessToken)) {
			if (!"".equals(responseJson)) {
				JsonNode rootNode = null;
				try {
					rootNode = new ObjectMapper().readTree(new StringReader(responseJson));

					if (rootNode != null) {
						accessToken = rootNode.at("/accessToken").asText();
						refreshToken = rootNode.at("/refreshToken").asText();
					}

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
    
    private void extractResponseTokens() {
    	List<String> domainsExtractAccessToken = new ArrayList<>();
    	
    	if(getProperty("domains.extract.access.token") != null)
    		domainsExtractAccessToken = Arrays.asList(getProperty("domains.extract.access.token").split(","));
    	
    	if(apiVersion == null || apiVersion.isEmpty()) {
			if (domain != null && !domain.isEmpty()) {
				apiVersion = getProperty(domain+".api.version");
				if(apiVersion == null || apiVersion.isEmpty())
					apiVersion = getProperty("api.version");
			}else {
				apiVersion = getProperty("api.version");
			}
		}

    	if (apiVersion != null && domainsExtractAccessToken.contains(apiVersion)) {
			extractAccessToken();
		}
    }
    
    private String generateRandomString() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    private String generateRandomNumber() {
        return Long.toString(Instant.now().toEpochMilli());
        //Random randomGenerator = new Random();
        //String randomInt = Integer.toString(randomGenerator.nextInt(1000000));
    }
    
    private String getCompletePath() {
    	String completePath = getProperty("host.schema") + "://" + getProperty("host.name") + ":"
				+ getProperty("host.port") + getProperty("api.version") + path;

		if (domain != null && !domain.isEmpty()) {
			if (getProperty(domain + ".host.name") != null && !getProperty(domain + ".host.name").isEmpty()) {
				completePath = getProperty(domain + ".host.schema") + "://" + getProperty(domain + ".host.name") + ":"
						+ getProperty(domain + ".host.port") + getProperty(domain + ".api.version") + path;
			}
    	}
		
		return completePath;
    }
}