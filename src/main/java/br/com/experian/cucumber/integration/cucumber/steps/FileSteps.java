package br.com.experian.cucumber.integration.cucumber.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class FileSteps extends MainSteps {

    private String path;
    private String fileName;


    @Given("^mainframe sends PEFIM file to \"([^\"]*)\"$")
    public void mainframeSendsPEFIMFileTo(String value) {
        path = value;
    }

    @And("^file name is \"([^\"]*)\"$")
    public void fileNameIs(String value) {
        fileName = value;
    }



    @And("^file has the following information:$")
    public void fileHasTheFollowingInformation(List<List<String>> table) throws IOException {
        System.out.println(table);
        BufferedWriter writer = new BufferedWriter(new FileWriter(path + "/" + fileName + ".txt", false));
        String str = "";
        for (List<String> row : table) {
            if (!row.get(0).toLowerCase().contains("description")) {
                str = row.toString();
                str = str.substring(str.indexOf(",")+2,str.length());
                str = str.replaceAll("(, )","").replace("]","").replaceAll("\"","");
                writer.append(str);
                writer.newLine();
            }
        }
        writer.close();
    }
}
