package br.com.experian.cucumber.integration.cucumber.steps;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import cucumber.api.Scenario;
import cucumber.api.java.Before;

public class Hooks {
	
	private static Collection<String> tags;
	public static Scenario scenario;
	
	private static boolean expectException;
    private static List<Exception> exceptions = new ArrayList<>();
    
	/**
	 * @throws Throwable
	 */
	@Before
	public void runBeforeWithOrder(Scenario scenario) throws Throwable {
		Hooks.scenario = scenario;
		keepScenarion(scenario);
	}

	public void keepScenarion(Scenario scenario) {
		Hooks.tags = scenario.getSourceTagNames();
	}
	
	public static void expectException() {
        expectException = true;
    }

    public static void add(Exception e) throws Exception {
        if (!expectException) {
            throw e;
        }
        exceptions.add(e);
    }

    public static List<Exception> getExceptions() {
        return exceptions;
    }
}

